
//publish the collection as you used to do with Mongo.Collection 
 Meteor.publish("userMyCollection", function()
 {
 	return UsersMysql.find();
 });
 
 Meteor.publish("achatMyCollection", function()
 {
    return AchatsMysql.find();
 });

Meteor.publish('userCollection', function()
{
 	return Users.find(); 
})

Meteor.publish('clientCollection', function()
{
	return Clients.find();
})

Meteor.publish('achatCollection', function()
{
	return Achats.find();
})

Meteor.publish('promocodeCollection', function()
{
	return Promocodes.find();
})


