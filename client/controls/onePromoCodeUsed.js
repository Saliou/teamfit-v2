Template.onePromocodeUsed.helpers({

	dateCode : function()
	{
		var d = new Date(this.Pro_dateCreation);

		return moment(d).format("LL");
	},

	discountType : function()
	{
		return this.Pro_type == "amount" ? "€" : "%";
	},
	Pro_value : function()
	{
		return numeral(this.Pro_value).format("0.00");
	},

})


Template.onePromocodeUsed.events({

	"click .promocodeDeleter" : function(e)
	{
		var promocodeId = $(e.target).parent().attr("data-promocodeId");

		var promocode = Promocodes.findOne({_id : promocodeId});
		
		Session.set("deletePromocode",promocode);
	},

	"click .promocodeEditer" : function(e)
	{
		var promocodeId = $(e.target).parent().attr("data-promocodeId");

		var promocode = Promocodes.findOne({_id : promocodeId});
		
		Session.set("editPromocode",promocode);
	},
})