Template.onePromocodeList.helpers({

	dateCode : function()
	{
		var d = new Date(this.Pro_dateCreation);

		return moment(d).format("LL");
	},

	discountType : function()
	{
		return this.Pro_type == "amount" ? "€" : "%";
	}
})

Template.onePromocodeList.events({

	"click .usePromocode" : function(e)
	{
		e.preventDefault();

		var promocodeId = $(e.target).parents("tr").attr("data-promocodeId");

		Meteor.call("usePromocode",promocodeId,function(err,res)
		{
			if(!err)
			{
				if( res == "promocode_doesnt_exist" )
				{
					sAlert.error("ce code promo n'existe pas");
				}
				else
				{
					sAlert.success("promocode mis a jour avec succès")
				}
			}	
			else
			{	sAlert.error("Une erreur est survenue lors de la modification du status du code promo");

				console.log(err.reason);
			}
		})
	}
})