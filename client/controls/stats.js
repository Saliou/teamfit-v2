/*
 * Play with this code and it'll update in the panel opposite.
 *
 * Why not try some of the options above?
 */

function dateAge( $age )
{
  var anneeActuelle = new Date().getFullYear();

  var dateVoulu = new Date((anneeActuelle-$age)+"-01-01");

  return dateVoulu;
}

Template.stats.rendered = function(){

  var acheteursData = 
  Morris.Bar({
  element: 'bar-age',
  data: [
    { y : '18-25 ans', a: 90, b: 10 },
    { y : '25-35 ans', a: 55,  b: 65 },
    { y : '+35 ans', a: 60,  b: 40 },
 
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Homme', 'Femme'],
  barColors : ["blue",'pink']
});

  Morris.Bar({
  element: 'bar-age',
  data: [
    { y : 'Saliou', a: 90},
    { y : 'SArr', a: 55 },
    { y : '+35 ans', a: 60 },
 
  ],
  xkey: 'y',
  ykeys: ['a'],
  labels: ['Prix',],
  barColors : ["red",]
});
}
Template.stats.helpers({

  inscrits : function(){ return Clients.find({}, {sort : {Cli_dateCreation : -1},limit : 10})},
 
  allInscrits : function(){ return Clients.find().count()},
  
  pourcentageHomme : function()
  {
    var all = Clients.find().count(); 
    
    var pop = Clients.find({Cli_sexe : "m"}).count();

    if( all == 0 ){ return 0; }

    var result = (pop/all) * 100;

    return numeral(result).format('0.00');

  },

  pourcentageFemme : function()
  {
    var all = Clients.find().count(); 
    
    var pop = Clients.find({Cli_sexe : "f"}).count();

    if( all == 0 ){ return 0; }

    var result = (pop/all) * 100;

    return numeral(result).format('0.00');

  },

  pourcentage18_25 : function()
  {
    var all = Clients.find().count(); 
    
    var pop = Clients.find({Cli_dateNaissance : {$lt : dateAge(18), $gt : dateAge(25) }}).count();

    if( all == 0 ){ return 0; }

    var result = (pop/all) * 100;

    return numeral(result).format('0.00');
  },

  pourcentage26_35 : function()
  {
    var all = Clients.find().count(); 
    
    var pop = Clients.find({Cli_dateNaissance : {$lt : dateAge(25), $gt : dateAge(35) }}).count();

    if( all == 0 ){ return 0; }

    var result = (pop/all) * 100;

    return numeral(result).format('0.00');
  },

  pourcentageUp_35 : function()
  {
    var all = Clients.find().count(); 
    
    var pop = Clients.find({Cli_dateNaissance : {$lt : dateAge(35) }}).count();

    if( all == 0 ){ return 0; }

    var result = (pop/all) * 100;

    return numeral(result).format('0.00');
  },
  achats : function()
  { 
    var achats = Achats.find({}, {sort : {Ach_prix : -1},limit : 10}).fetch();
    
    var result = [];
    
    for (var i = 0; i < achats.length; i++)
    {
      result[i] = { achat : achats[i],

      client : Clients.findOne({_id : achats[i].Ach_clientId}) }
    }
      
    console.log(result);

    return result;

  },

  ventesTotal : function()
  {
    var achats = Achats.find().fetch();
    
    var total = 0;
    
    for (var i = 0; i < achats.length; i++)
    {
       total+= achats[i].Ach_prix;
    }

    return {total : numeral(total).format('0.00'), nbAchats : Achats.find().count()}
  },
})