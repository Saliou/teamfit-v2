function dateAge( $age )
{
	var anneeActuelle = new Date().getFullYear();

	var dateVoulu = new Date((anneeActuelle-$age)+"-01-01");

	return dateVoulu;
}

Template.welcome.helpers({

	inscrits : function(){ return  Clients.find().count(); },
	
	pourcentageHomme : function()
	{
		var all = Clients.find().count(); 
		
		var pop = Clients.find({Cli_sexe : "m"}).count();

		if( all == 0 ){ return 0; }

		var result = (all/pop) * 100;

		return numeral(result).format('0.00');


	},

	pourcentageFemme : function()
	{
		var all = Clients.find().count(); 
		
		var pop = Clients.find({Cli_sexe : "f"}).count();
 
		if( all == 0 ){ return 0; }

		return percentage.from(pop,all);
	},

	pourcentage18_25 : function()
	{
		var all = Clients.find().count(); 
		
		var pop = Clients.find({Cli_dateNaissance : {$gt : dateAge(18), $lt : dateAge(25) }}).count();

		if( all == 0 ){ return 0; }

		return percentage.from(pop,all);
	},

	pourcentage26_35 : function()
	{
		var all = Clients.find().count(); 
		
		var pop = Clients.find({Cli_dateNaissance : {$gt : dateAge(25), $lt : dateAge(35) }}).count();

		if( all == 0 ){ return 0; }

		return percentage.from(pop,all);
	},

	pourcentageUp_35 : function()
	{
		var all = Clients.find().count(); 
		
		var pop = Clients.find({Cli_dateNaissance : {$gt : dateAge(35) }}).count();

		if( all == 0 ){ return 0; }

		return percentage.from(pop,all);
	},

})