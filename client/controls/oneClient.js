Template.oneClient.helpers({

	age : function(){ return toAge(new Date(this.Cli_dateNaissance));},
	
	sms : function(){ return this.Cli_sms ? "glyphicon glyphicon-ok" : "glyphicon glyphicon-remove"},
	
	newsletter : function(){ return this.Cli_newsletter ? "glyphicon glyphicon-ok" : "glyphicon glyphicon-remove"},

	sexe : function(){ return this.Cli_sexe == "m" ? "Homme" : "Femme"},
})

Template.oneClient.events({

	'click .rowClick' : function(e)
	{
		e.preventDefault();

		var dataId = $(e.target).parent().attr("data-clientId");

		Router.go("clientPage",{_id : dataId});
	}
})