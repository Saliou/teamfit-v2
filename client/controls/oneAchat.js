Template.oneAchat.helpers({

	dateAchat : function()
	{
		var d = new Date(this.Ach_dateAchat);

		return moment(d).format("LL");
	},

	discount : function()
	{
		return this.Ach_discount ? this.Ach_discount+"%" : "--"
	},
	discountType : function()
	{
		var $dT = {"10eachat" : "10e Achat"};
		
		return $dT[this.Ach_discountType];
	}
})