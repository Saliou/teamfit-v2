
	var jsonLoader = function()
	{
		var saliou = $.getJSON("/teamfit.json",function(data)
		{
			var allAchats = data.achats;
			
			var allClients = data.clients;

			for (var i = 0; i < allClients.length; i++)
			{
				var clientToInsert = 
				{
					Cli_nom : allClients[i].Use_nom,

					Cli_prenom : allClients[i].Use_prenom,

					Cli_dateNaissance : new Date(allClients[i].Use_dateNaissance),

					Cli_sexe : allClients[i].Use_sexe,

					Cli_telephone : allClients[i].Use_telephone,

					Cli_email : allClients[i].Use_email,

					Cli_adresse : allClients[i].Use_adresse,

					Cli_ville : allClients[i].Use_ville,

					Cli_codePostal : allClients[i].Use_codePostal,

					Cli_newsletter : Boolean(allClients[i].Use_newsletter),

					Cli_sms : Boolean(allClients[i].Use_sms),

					Cli_points : parseFloat(allClients[i].Use_points),

					Cli_dateCreation : new Date(allClients[i].Use_dateCreation),

					Cli_jauge : 0,

				}

				//Test de l'existence de la base de données

				var clientExist = Clients.find({Cli_email : allClients[i].Use_email}).count() > 0;

				if( clientExist ) { continue; }

				/* A partir de là ca veut dire que le client n'est pas dans la base de données */

				var idClient = Clients.insert(clientToInsert);

				for (var j = 0; j < allAchats.length; j++)
				{
					if( allAchats[j].Ach_userId == allClients[i].Use_id && allAchats[j].Ach_deleted == 0  )
					{
						var achatToInsert = 
						{
							Ach_clientId : idClient,

							Ach_prix : parseFloat(allAchats[j].Ach_price),

							Ach_dateAchat : new Date(allAchats[j].Ach_dateCreation)
						};

						Meteor.call("addAchatMysql",achatToInsert,function(err,res)
						{
							console.log(res);

							if(err){ console.log(err)}  
						})
					}
						
				}
			}
			
		});
	}

Template.listeClients.rendered = function(template) {

	//jsonLoader();

	$('#editClient').on('show.bs.modal', function (e){

  		var clientId = $(e.relatedTarget).parent().parent().parent().attr("data-clientId");
  		
  		Session.set("editClient",Clients.findOne({_id : clientId}));
	})

	$('#deleteClient').on('show.bs.modal', function (e){

  		var clientId = $(e.relatedTarget).parent().parent().parent().attr("data-clientId");
  		
  		Session.set("deleteClient",Clients.findOne({_id : clientId}));
	})
}

Template.listeClients.helpers({

	clients : function()
	{		
		if( Session.get("listeClients") ){ return Session.get("listeClients")}
	},
  	
  	inscrits : function(){ return Clients.find({}, {sort : {Cli_dateCreation : -1},limit : 10})},

	
	hasClient : function()
	{
		if( Session.get('listeClients') )
		{
			return Session.get('listeClients').length > 0;
		}
		else
		{
			return false;
		}
	}
	
});

Template.listeClients.events({

	'submit .searchClient' : function(e)
	{
		e.preventDefault();

		var q = $(e.target).find("[name=query]").val();

		var query = { $regex : q , $options	 : "i"}

		Session.set("listeClients",Clients.find(
			{ 
				$or : 
				[
					{Cli_nom : query},
					{Cli_prenom : query},
					{Cli_email : query},
					{Cli_telephone : query}
				]
			}).fetch());

	},

	'keyup #query' : function(e)
	{
		e.preventDefault();

		var q = $("#query").val();

		var query = { $regex : q , $options	 : "i"}

		Session.set("listeClients",Clients.find(
			{ 
				$or : 
				[
					{Cli_nom : query},
					{Cli_prenom : query},
					{Cli_email : query},
					{Cli_telephone : query}
				]
			}).fetch());	
	},
	
	'click .filter' : function(e)
	{
		e.preventDefault();

		var filter = $(e.target).attr("data-filter");

		Session.set("listeClients",Clients.find(
			{ 
				$or : [ {Cli_nom : query}, {Cli_prenom : query}, {Cli_email : query},{Cli_telephone : query} ]
			},
			{$sort : {filter : 1}}).fetch());
	}
	
})