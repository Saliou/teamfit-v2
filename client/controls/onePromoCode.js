Template.onePromocode.helpers({

	dateCode : function()
	{
		var d = new Date(this.Pro_dateCreation);

		return moment(d).format("LL");
	},

	discountType : function()
	{
		return this.Pro_type == "amount" ? "€" : "%";
	},
	Pro_value : function()
	{
		return numeral(this.Pro_value).format("0.00");
	},

})

Template.onePromocode.events({

	"click .promocodeDeleter" : function(e)
	{
		var promocodeId = $(e.target).parent().attr("data-promocodeId");

		var promocode = Promocodes.findOne({_id : promocodeId});
		
		Session.set("deletePromocode",promocode);
	},

	"click .promocodeEditer" : function(e)
	{
		var promocodeId = $(e.target).parent().attr("data-promocodeId");

		var promocode = Promocodes.findOne({_id : promocodeId});
		
		Session.set("editPromocode",promocode);
	},

	"click .promocodeUser" : function(e)
	{
		e.preventDefault();

		var promocodeId = $(e.target).parent().attr("data-promocodeId");
		
		Meteor.call("usePromocode",promocodeId,function(err,res)
		{
			if( !err )
			{
				if( res == "promocode_doesnt_exist"  )
				{
					sAlert.error("Ce code promo n'existe pas");
				}
				else
				{
					sAlert.success("Code promo mis à jour avec succès");
				}
			}
			else
			{
				sAlert.error("Une erreur est survenue lors de la mise à jour du code promo");

				console.log("Use Promocode E : "+err.reason);
			}
		})
	}
})