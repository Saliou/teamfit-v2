Template.promocodes.helpers({

	noPromocodes : function()
	{
		return Promocodes.find().count() <= 0;
	},
	promocodesNoUsed : function()
	{
		return Promocodes.find({Pro_used : false}).fetch();
	},
	promocodesUsed : function()
	{
		return Promocodes.find({Pro_used : true}).fetch();
	},

})