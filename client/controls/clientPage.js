Template.clientPage.rendered = function(a){


	$('#editClient').on('show.bs.modal', function (e){

  		var clientId = $(e.relatedTarget).parent().parent().attr("data-clientId");

  		Session.set("editClient",Clients.findOne({_id : clientId}));
	});

	$('#deleteClient').on('show.bs.modal', function (e){

  		var clientId = $(e.relatedTarget).parent().parent().attr("data-clientId");
  		
  		Session.set("deleteClient",Clients.findOne({_id : clientId}));
	});

  $('#editAchat').on('show.bs.modal', function (e){

      var achatId = $(e.relatedTarget).parent().parent().attr("data-achatId");
      
      Session.set("editAchat",Achats.findOne({_id : achatId}));
  });

  $('#deleteAchat').on('show.bs.modal', function(e){

      var achatId = $(e.relatedTarget).parent().parent().attr("data-achatId");      
      
      Session.set("deleteAchat",Achats.findOne({_id : achatId}));

  });

  $('#editPromocode').on('show.bs.modal', function (e){

      var promocodeId = $(e.relatedTarget).parent().parent().attr("data-promocodeId");
      
      Session.set("editPromocode",Promocodes.findOne({_id : promocodeId}));
  });

  $('#deletePromocode').on('show.bs.modal', function(e){

      var promocodeId = $(e.relatedTarget).parent().parent().attr("data-promocodeId");      
      
      Session.set("deletePromocode",Promocodes.findOne({_id : promocodeId}));

  });
}

Template.clientPage.helpers({

	age : function()
	{
		return toAge(new Date(this.client.Cli_dateNaissance));
	},

	dateNaissance : function()
	{
	 	var d = new Date(this.client.Cli_dateNaissance);
    
	 	return moment(d).format('LL'); 
	},

  promocodes : function()
  {
    return Promocodes.find({"Pro_client._id" : this.client._id  },{sort : { Pro_used : 1 } });
  },

  nombreAchats : function()
  {
    return this.achats.count()
  },

  totalAchats : function()
  {
    var total = 0;

    var achats = Achats.find({Ach_clientId : this.client._id}).fetch();
    
    for (var i = 0; i < achats.length; i++)
    {
       total+= achats[i].Ach_prix;
    }

    return numeral(total).format('0.00');

  },
  percent : function()
  {
    return this.achats.count() * 10
  },

  left : function()
  {
    return 10 - this.achats.count()
  },
  sexe : function()
  {
     return this.client.Cli_sexe == "m" ? "Homme" : "Femme";
  },

})