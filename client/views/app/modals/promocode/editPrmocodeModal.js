Template.editPromocodeModal.helpers({

	promocode : function(){ return Session.get("editPromocode"); },

	datePromocode : function()
	{ 
		if( !Session.get("editPromocode") ) return "";

		var d = new Date(Session.get("editPromocode").Pro_datePromocode);

		return moment(d).format("DD/MM/YYYY");
	},
	typeChoicePercent : function()
	{
		if( !Session.get("editPromocode") ) return "";
		
		return Session.get("editPromocode").Pro_type == "percent" ? "selected" : "";
	},
	typeChoiceAmount : function()
	{
		if( !Session.get("editPromocode") ) return "";

		return Session.get("editPromocode").Pro_type == "amount" ? "selected" : "";
	},
});

Template.editPromocodeModal.events({

	'submit .edit-promocode' : function(e)
	{
		e.preventDefault();

		var promocode =
		{
			Pro_id : Session.get("editPromocode")._id,

			Pro_value : parseFloat($(e.target).find("[name=promocodeValue]").val()),

			Pro_type : $(e.target).find("[name=promocodeType]").val(),
			
			Pro_description : $(e.target).find("[name=promocodeDescription]").val(),
		
		}

		//S'assurer que la value soit un nombre et date soit valide;

		Meteor.call('editPromocode',promocode,function(err,res)
		{
			if(!err)
			{
				if( res == "promocode_dont_exist")
				{
					alert("Ce code promo n'existe plus");
				}
				
				else
				{
					$('#editPromocode').modal('hide');
				}
			}
			else
			{
				alert("edit Promocode Error : "+err.reason);
			}
		})
	},
	
	
})
