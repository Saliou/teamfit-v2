Template.deletePromocodeModal.helpers({

	promocode : function(){ return  Session.get("deletePromocode"); }

});

Template.deletePromocodeModal.events({

	'click .deletePromocode' : function(e)
	{
		e.preventDefault();
		
		var promocodeId = Session.get("deletePromocode")._id;

		Meteor.call('deletePromocode',promocodeId,function(err,res)
		{
			if(!err)
			{
				console.log(res);

				$('#deletePromocode').modal('hide');
			}
			else
			{
				alert("del Error : "+err.reason);
			}
		})
	}
})

