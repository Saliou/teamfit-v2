Template.addPromocodeModal.helpers({


});


Template.addPromocodeModal.events({

	'submit .add-promocode' : function(e)
	{
		e.preventDefault();

		var promocode =
		{
			Pro_value : parseFloat( $(e.target).find("[name=promocodeValue]").val() ),

			Pro_code : $(e.target).find("[name=promocodeCode]").val(),
			
			Pro_type : $(e.target).find("[name=promocodeType]").val(),

			Pro_client : Clients.findOne({_id : Template.currentData().client._id }),

			Pro_description : $(e.target).find("[name=promocodeDescription]").val(),

		};

		Meteor.call('addPromocode',promocode,function(err,res)
		{
			if(!err)
			{
				if( res == "client_dont_exist" )
				{
					alert('ddd');
				}
				else
				{
					$('#addPromocode').modal('hide');
				}

			}
			else
			{
				alert("edit Promocode Error : "+err.reason);
			}
		})
	},
	'click .generateCode' : function(e)
	{
		e.preventDefault();

		var code = randomstring.generate(
 		{
		  length: 6,
		  
		  charset: 'alphabetic',
		  
		  capitalization : "uppercase",

		});

		$("input[name=promocodeCode]").val( code );
	}
	
	
})
