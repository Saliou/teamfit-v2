Template.deleteAchatModal.helpers({

	achat : function(){ return  Session.get("deleteAchat"); }

});

Template.deleteAchatModal.events({

	'click .deleteAchat' : function(e)
	{
		e.preventDefault();
		
		var achatId = Session.get("deleteAchat")._id;

		Meteor.call('deleteAchat',achatId,function(err,res)
		{
			if(!err)
			{
				console.log(res);

				$('#deleteAchat').modal('hide');
			}
			else
			{
				alert("del Error : "+err.reason);
			}
		})
	}
})

