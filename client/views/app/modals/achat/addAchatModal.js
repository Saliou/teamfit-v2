Template.addAchatModal.helpers({

	hasPromocode : function()
	{
		var promoccodesToUse =  Promocodes.find({Pro_clientId : Template.currentData().client._id,Pro_used : false });

		return promoccodesToUse.count() > 0 ? promoccodesToUse : false;
	}

});


Template.addAchatModal.events({

	'submit .add-achat' : function(e)
	{
		e.preventDefault();
		
		var achat =
		{
			Ach_prix : parseFloat( $(e.target).find("[name=prix]").val() ),

			Ach_clientId : Template.currentData().client._id,
		};

		Meteor.call('addAchat',achat,function(err,res)
		{
			if(!err)
			{
				if( res == "client_dont_exist" )
				{
					alert('ddd');
				}
				else
				{
					$('#addAchat').modal('hide');
				}

			}
			else
			{
				alert("edit Achat Error : "+err.reason);
			}
		})
	},
	
	
})
