Template.editAchatModal.helpers({

	achat : function(){ return Session.get("editAchat"); },

	dateAchat : function()
	{ 
		if( !Session.get("editAchat") ) return "";

		var d = new Date(Session.get("editAchat").Ach_dateAchat);

		return moment(d).format("DD/MM/YYYY");
	},
});

Template.editAchatModal.events({

	'submit .edit-achat' : function(e)
	{
		e.preventDefault();

		var achat =
		{
			Ach_id : Session.get("editAchat")._id,

			Ach_prix : parseFloat($(e.target).find("[name=prix]").val()),
			
			Ach_dateAchat : new Date($(e.target).find("[name=dateAchat]").val()),
		
		}

		//S'assurer que prix soit un nombre et date soit valide;	
		Meteor.call('editAchat',achat,function(err,res)
		{
			if(!err)
			{
				if( res == "achat_dont_exist")
				{
					alert("ahcat dont existd");
				}
				
				else
				{
					$('#editAchat').modal('hide');
				}
			}
			else
			{
				alert("edit Achat Error : "+err.reason);
			}
		})
	},
	
	
})
