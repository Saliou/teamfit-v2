Template.nav.helpers({

  nombreClients : function(){ return Clients.find({}).count()},
  
  promocodesLeft : function()
  { 
    var promoCount = Promocodes.find({Pro_used : false}).count();

    return promoCount > 0  ? promoCount : false;
  },
  
  isAdmin : function(){ return (Meteor.user() && Meteor.user().profile.type == "admin" )},

  nom : function()
  {
    return Meteor.user().profile.nom
  },
  
  prenom : function()
  {
    return Meteor.user().profile.prenom
  },

  inscrits : function(){ return Clients.find({}, {sort : {Cli_dateCreation : -1},limit : 4})},

  achats : function()
  { 
    var achats = Achats.find({}, {sort : {Ach_dateAchat : -1},limit : 4}).fetch();
    
    var result = [];
    
    for (var i = 0; i < achats.length; i++)
    {
      result[i] = { achat : achats[i],

      client : Clients.findOne({_id : achats[i].Ach_clientId}) }
    }

    return result;

  },
}) 

Template.layout.events({

  'submit .searchClient' : function(e)
  {
    e.preventDefault();

    var q = $(e.target).find("[name=query]").val();

    var query = { $regex : q , $options  : "i"}

    Session.set("listeClients",Clients.find(
      { 
        $or : 
        [
          {Cli_nom : query},
          {Cli_prenom : query},
          {Cli_email : query},
          {Cli_telephone : query}
        ]
      }).fetch());

  },

  'keyup #query' : function(e)
  {
    e.preventDefault();

    var q = $("#query").val();

    var query = { $regex : q , $options  : "i"}

    Session.set("listeClients",Clients.find(
      { 
        $or : 
        [
          {Cli_nom : query},
          {Cli_prenom : query},
          {Cli_email : query},
          {Cli_telephone : query}
        ]
      }).fetch());  

    Router.go("listeClients");
  },

  'click .disconnectButton' : function(e)
  {
    e.preventDefault();

    Accounts.logout(function(e)
    {
      if(!e)
      {
        Router.go("home");
      }
      
      else
      {
        alert("Erreur lors de deconnexion");

        consoole.log("##disconnect => "+e.reason);
      }
    })
  }
})

