//*= Change parameter 
module.exports = {
  servers: {
    one: {
      host: '149.202.46.227',//* URL server
      username: 'root',
      password: '1qEzuD5c'//* Put root password 
    }
  },

  meteor: {
    name: 'TeamFit',//* Set meteor project name
    path: '../../teamfit',
    port : 3100,
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      PORT: 80,
      ROOT_URL: 'http://saliousarr.com',//* URL Server
      MONGO_URL: 'mongodb://localhost/meteor'
    },

    dockerImage: 'abernix/meteord:base',//* use meteorhacks/meteord only for project under Meteor 1.4
    deployCheckWaitTime: 120
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};