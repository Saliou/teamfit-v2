Achats = new Meteor.Collection("achats");

Meteor.methods({

	addAchat : function( achat )
	{
		check(achat,
		{
			Ach_clientId : String,

			Ach_prix : Number,
		});

		var clientExist = Clients.findOne({_id : achat.Ach_clientId});

		if( !clientExist ) { return "client_doesnot_exist"}

	 	var newJauge = Clients.findOne({_id : achat.Ach_clientId}).Cli_jauge + 1;
	 	
	 	var newPoints = Math.round(Clients.findOne({_id : achat.Ach_clientId}).Cli_points + achat.Ach_prix / 10);
	 	
	 	var discountTaux = 5;

	 	var achatToInsert = _.extend(achat,
	 	{
	 		Ach_dateAchat : new Date(),
	 	})
	 	
	 	if( newJauge >= 10 )
	 	{
	 		newJauge = 0;

	 		var total = 0;

	 		var achatsClient = Achats.find({Ach_clientId : achat.Ach_clientId}).fetch();

	 		for (var i = 0; i < achatsClient.length; i++)
	 		{
	 			total += achatsClient[i].Ach_prix;	
	 		}

	 		var discount = Math(total / 100 ) * discountTaux;
	 		
	 		discount = Math.round(discount * 100)/100;

	 		var code = randomstring.generate(
	 		{
			  length: 6,
			  
			  charset: 'alphabetic',
			  
			  capitalization : "uppercase",

			});

	 		var promocode = { 

	 			Pro_value : discount,
	 			Pro_type : "amount",
	 			Pro_code : code,
	 			Pro_client : clientExist,
	 			Pro_description : "Bons pour 10 achats chez Teamfit"
	 		};

	 		Meteor.call("addPromocode",promocode,function(err,res)
	 		{
	 			if( !err )
	 			{
	 				if( res == 1 )
	 				{
	 					console.log("good promocode");
	 				}
	 			}
	 			else
	 			{
	 				console.log("Promocode insert error");
	 				
	 				console.log(err);
	 			}
	 		})
	 		
	 		Clients.update(achat.Ach_clientId,{$set : {Cli_jauge : newJauge,Cli_points : newPoints }})

	 		var insertAchat = Achats.insert(achatToInsert);

	 		if( insertAchat )
	 		{
	 			return "10_achats";
	 		}
	 	}

	 	Clients.update(achat.Ach_clientId,{$set : {Cli_jauge : newJauge,Cli_points : newPoints }})

	 	return Achats.insert(achatToInsert);
	},

	addAchatMysql : function( achat )
	{
		check(achat,
		{
			Ach_clientId : String,

			Ach_prix : Number,
			
			Ach_dateAchat : Date,
		});

		var clientExist = Clients.findOne({_id : achat.Ach_clientId});

		if( !clientExist ) { return "client_doesnot_exist"}

	 	var newJauge = Clients.findOne({_id : achat.Ach_clientId}).Cli_jauge + 1;
	 	
	 	var newPoints = Math.round(Clients.findOne({_id : achat.Ach_clientId}).Cli_points + achat.Ach_prix / 10);
	 	
	 	var discountTaux = 5;

	 	
	 	if( newJauge >= 10 )
	 	{
	 		newJauge = 0;

	 		var total = 0;

	 		var achatsClient = Achats.find({Ach_clientId : achat.Ach_clientId}).fetch();

	 		for (var i = 0; i < achatsClient.length; i++)
	 		{
	 			total += achatsClient[i].Ach_prix;	
	 		}

	 		var discount = (total / 100 ) * discountTaux;
	 			 		
	 		var code = randomstring.generate(
	 		{

			  length: 6,
			  charset: 'alphabetic',
			  capitalization : "uppercase",

			});

	 		var promocode = { 

	 			Pro_value : discount,
	 			Pro_type : "amount",
	 			Pro_code : code,
	 			Pro_client : clientExist,
	 			Pro_description : "Bons pour 10 achats chez Teamfit"
	 		};

	 		Meteor.call("addPromocode",promocode,function(err,res)
	 		{
	 			if( !err )
	 			{
	 				if( res == 1 )
	 				{
	 					console.log("good promocode");
	 				}
	 			}
	 			else
	 			{
	 				console.log("Promocode insert error");
	 				
	 				console.log(err);
	 			}
	 		})
	 		
	 		Clients.update(achat.Ach_clientId,{$set : {Cli_jauge : newJauge,Cli_points : newPoints }})

	 		var insertAchat = Achats.insert(achat);

	 		if( insertAchat )
	 		{
	 			return "10_achats";
	 		}
	 	}

	 	Clients.update(achat.Ach_clientId,{$set : {Cli_jauge : newJauge,Cli_points : newPoints }})

	 	return Achats.insert(achat);
	},	

	editAchat : function( achat )
	{
		check(achat,
		{
			Ach_id : String,
			Ach_prix : Number,	
			Ach_dateAchat : Date
		});	

		var achatExist = Achats.findOne({_id : achat.Ach_id});

		if( !achatExist ){ return "achat_doesnt_exist" };

		var newPoints = Clients.findOne({_id : achat.Ach_clientId}).Cli_points;

		newPoints -= achatExist.Ach_prix / 10;

		newPoints += achat.Ach_prix /10 ;

	 	Clients.update(achat.Ach_clientId,{$set : {Cli_points : newPoints }});

	 	return Achats.update(achat.Ach_id, { $set : achat});
	},

	deleteAchat : function( achatId )
	{
		check(achatId,String);

		var hasAchat = Achats.findOne({_id : achatId});

		if( !hasAchat ){ return "achat_doesnt_exist";}

		var newJauge = Clients.findOne({_id : hasAchat.Ach_clientId}).Cli_jauge;

		newJauge -= 1;

		newJauge = newJauge < 0 ? 0 : newJauge;

		var newPoints = Clients.findOne({_id : hasAchat.Ach_clientId}).Cli_points;

		newPoints -= hasAchat.Ach_prix / 10 ;

	 	Clients.update(hasAchat.Ach_clientId,{$set : {Cli_jauge : newJauge,Cli_points : newPoints }});

		return Achats.remove(hasAchat);
	},
})



