Promocodes = new Meteor.Collection("promocodes");

//_id, montant (ou le pourcentage),type,seen (genre par teamfit) code, dateCreation , clientId ,used	

Meteor.methods({

	seenPromocode : function()
	{
		Promocodes.update({Pro_seen : false},{$set : { Pro_seen : true}});

	},
	addPromocode : function( promocode )
	{
		check(promocode,
		{
			Pro_value : Number,
			Pro_type : String, //Percent or amount
			Pro_code : String,
			Pro_client : Object,
			Pro_description : String,
		})

		promocode.Pro_description = promocode.Pro_description == "" ? "Sans description" : promocode.Pro_description;

		promocode.Pro_value = Math.round( promocode.Pro_value * 100 )  / 100;
		
		//var existPromoCode = Promocodes.findOne({ Pro_code : promocode.Pro_code });

		//if( existPromoCode ) { return "promocode_exist" }

		var codeToInsert = _.extend(promocode,{

			Pro_seen : false,
			Pro_used : false,
			Pro_dateCreation : new Date(),
		})

		return Promocodes.insert(codeToInsert);
	},

	editPromocode : function( promocode )
	{
		check(promocode,
		{
			Pro_id : String,
			Pro_value : Number,
			Pro_type : String, //Percent or amount
			Pro_description : String,
		});	

		var promocodeExist = Promocodes.findOne({_id : promocode.Pro_id});
		
		if( !promocodeExist ){ return "promocode_doesnt_exist" };

		var promocodeToupdate = _.extend(promocode,
		{
			Pro_edited : true
		})
		
		promocodeToupdate = _.omit(promocode,"Pro_id")

	 	return Promocodes.update(promocode.Pro_id, { $set : promocodeToupdate});
	},

	usePromocode : function( promocodeId )
	{
		check(promocodeId,String);

		var hasPromocode = Promocodes.findOne({_id : promocodeId});

		if( !hasPromocode ){ return "promocode_doesnt_exist";}

		return Promocodes.update(promocodeId,{$set : {Pro_used : true}});
	},

	deletePromocode : function( promocodeId )
	{
		check(promocodeId,String);

		var hasPromocode = Promocodes.findOne({_id : promocodeId});

		if( !hasPromocode ){ return "promocode_doesnt_exist";}

		return Promocodes.remove(hasPromocode);
	},
})