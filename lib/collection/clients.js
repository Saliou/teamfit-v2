Clients = new Meteor.Collection("clients");

Meteor.methods({

	addClient : function( client)
	{
		check(client,
		{
			Cli_nom : String,
			Cli_prenom : String,
			Cli_dateNaissance : Date,
			Cli_sexe : String,
			Cli_telephone : String,
			Cli_email : String,
			Cli_adresse : String,
			Cli_ville : String,
			Cli_codePostal : String,
			Cli_newsletter : Boolean,
			Cli_sms : Boolean
		});

		var hasTelephone = Clients.find({Cli_telephone : client.Cli_telephone,Cli_email : client.Cli_email}).count();
	 	
	 	var hasMail = Clients.find({Cli_email : client.Cli_email,Cli_email : client.Cli_email}).count();

	 	if( hasTelephone > 0){ return "telephone_exist"; }

	 	if( hasMail > 0){ return "mail_exist"; }

	 	var clientToInsert = _.extend(client,
	 	{
	 		Cli_dateCreation : new Date(),
	 		Cli_points : 0,
	 		Cli_jauge : 0,
	 	})

	 	return Clients.insert(clientToInsert);
	},

	editClient : function( client )
	{
		check(client,
		{
			Cli_id : String,
			Cli_nom : String,
			Cli_prenom : String,
			Cli_dateNaissance : Date,
			Cli_sexe : String,
			Cli_telephone : String,
			Cli_email : String,
			Cli_adresse : String,
			Cli_ville : String,
			Cli_codePostal : String,
			Cli_newsletter : Boolean,
			Cli_sms : Boolean
		});

	 	var hasTelephone = Clients.find({_id : {$ne : client.Cli_id},Cli_telephone : client.Cli_telephone,Cli_email : client.Cli_email}).count();
	 	
	 	var hasMail = Clients.find({_id : {$ne : client.Cli_id},Cli_email : client.Cli_email,Cli_email : client.Cli_email}).count();

	 	if( hasTelephone > 0){ return "telephone_exist"; }

	 	if( hasMail > 0){ return "mail_exist"; }

	 	return Clients.update(client.Cli_id,{ $set : client});
	},

	deleteClient : function( clientId )
	{
		check(clientId,String);

		var hasClient = Clients.findOne({_id : clientId});

		if( !hasClient ){ return "client_doesnot_exist";}

		Achats.remove({Ach_clientId : clientId});

		return Clients.remove(hasClient);
	},
})