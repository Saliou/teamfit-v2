Router.configure({

	layoutTemplate : "layout",
	
	notFoundTemplate : "404",
	
	loadingTemplate : "loading",
	
	waitOn : function(){ return Meteor.subscribe("userCollection") && Meteor.subscribe("clientCollection") && Meteor.subscribe("achatCollection") && Meteor.subscribe("promocodeCollection") }
})

Router.route('/',{
	
	name : "home",
	
	waitOn : function(){ return Meteor.subscribe("userCollection") },
});

Router.route('client/:_id',{

	name : "clientPage" ,

	waitOn : function(){ return Meteor.subscribe("clientCollection")  && Meteor.subscribe("achatCollection") && Meteor.subscribe("promocodeCollection") },

	action : function()
	{
		var client = Clients.findOne({_id : this.params._id})

		if( client )
		{
			var achats = Achats.find({Ach_clientId : this.params._id})

			this.render("clientPage",{ data : function(){ return { client : client,achats : achats} } });
		}
		else
		{
			this.render("noClient");
		}
	}
});

Router.route('promocodes',{

	name : "promocodes" ,

	waitOn : function(){ return Meteor.subscribe("promocodeCollection") && Meteor.subscribe("clientCollection") },

});

Router.route('clients/',{

	name : "listeClients",
	
	waitOn : function(){ return Meteor.subscribe("clientCollection")  },

	action : function()
	{ 
		
		Session.set("listeClients",Clients.find({},{sort : { Cli_dateCreation : -1 },limit : 50}).fetch());

		this.render();
	} 
});

Router.route('construction',{

	name : "construction"
})

Router.route('stats',{

	name : "stats",

	waitOn : function(){ return Meteor.subscribe("clientCollection") && Meteor.subscribe("achatCollection")  }
});

var isLogged = function()
{
   if( !Meteor.userId() )
   {
   	this.render("login");
   }

   else
   {
   	this.next()
   }
}


Router.onBeforeAction(isLogged);